What is this?
=================
This is a web app using Heroku with golang.

How to build a web app using Heroku with golang?
---------------------------------------
To start-up, follow this tutorial:
[Here] (https://medium.com/@freeformz/hello-world-with-go-heroku-38295332f07b)

Connect Heroku with your github repository
---------------------------------------
Now your web app has been deployed at Heroku. It is simple, right? But the bad part is, although you used git, your code stores at Heroku and the modifications cannot be shown as same as a github repository. Fortunately, your web app at Heroku and a github repo could be magically linked together. You are only three steps left to archive this:

1. Create a new repo at github.com. For example: my-heroku-webapp
2. Duplicate your repo and push to github. Follow this guide to figure out how to do it: https://help.github.com/articles/duplicating-a-repository/
3. Go to https://dashboard.heroku.com/apps and find your web app. The app name is randomly generated in a format of: aaaaa-bbbbb-1234

  Click on deploy ---> Github ---> Connect to Github <br/>
  Enter my-heroku-webapp and click Search <br/>
  Click Enable Automatic Deploys

Now you can push to github in the rest of your life.
